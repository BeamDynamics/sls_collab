function [x,y,sigmax,sigmay] = analyzeBeamProfile(im)

if isstruct(im)% im is a struct. We assume all fields are matricies with images
    fnames = fieldnames(im);
    for i = 1:numel(fnames)% Iterate over all fields (i.e. cameras)
        im_mat = getfield(im,fnames{i});
        [x(i),sigmax(i),~,~] = gaussianfit1D(sum(double(im_mat),1),0);
        [y(i),sigmay(i)] = gaussianfit1D(sum(double(im_mat),2),0);
    end
    
elseif ismatrix(im)% im is already the image
    [x,sigmax] = gaussianfit1D(sum(double(im),1),0);
    [y,sigmay] = gaussianfit1D(sum(double(im),2),0);
else
    error('Input variable format not recognized');
end