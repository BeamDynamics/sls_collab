clear all
addpath('..');
fnames_h = {...
    'CalDip4_CH6_m2A_06-Dec-2020_15-21-07';...
    'CalDip4_CH6_m1A_06-Dec-2020_15-18-30';...
    'CalDip4_NominalSteerers_06-Dec-2020_14-49-28';...
    'CalDip4_CH6_p1A_06-Dec-2020_15-16-44';...
    'CalDip4_CH6_p2A_06-Dec-2020_15-22-35'};


fnames_v = {...
    'CalDip4_CV6_m2A_06-Dec-2020_15-29-51';...
    'CalDip4_CV6_m1A_06-Dec-2020_15-25-37';...
    'CalDip4_NominalSteerers_06-Dec-2020_14-49-28';...
    'CalDip4_CV6_p1A_06-Dec-2020_15-24-09';...
    'CalDip4_CV6_p2A_06-Dec-2020_15-28-29'};

for i = 1:numel(fnames_h)
   
    i
    load(fnames_h{i})
    for a = 1:numel(d5)
        [x_tmp(a),sigmax_tmp(a),~,~] = gaussianfit1D(sum(double(d4{a})),0);
        [y_tmp(a),sigmay_tmp(a),~,~] = gaussianfit1D(sum(double(d4{a}),2),0);
    end
    xx(i) = mean(rmoutliers(x_tmp));% Horizontal movement due to horizontal steerer
    xx_std(i) = std(rmoutliers(x_tmp));
    yx(i) = mean(rmoutliers(y_tmp));% Vertical movement due to horizontal steerer
    yx_std(i) = std(rmoutliers(y_tmp));
end

for i = 1:numel(fnames_v)
    i
    load(fnames_v{i})
    for a = 1:numel(d5)
        [x_tmp(a),sigmax_tmp(a),~,~] = gaussianfit1D(sum(double(d4{a})),0);
        [y_tmp(a),sigmay_tmp(a),~,~] = gaussianfit1D(sum(double(d4{a}),2),0);
    end
    xy(i) = mean(rmoutliers(x_tmp));% Horizontal movement due to vertical steerer
    xy_std(i) = std(rmoutliers(x_tmp));
    yy(i) = mean(rmoutliers(y_tmp));% Vertical movement due to vertical sterrer
    yy_std(i) = std(rmoutliers(y_tmp));
end
%%
figure('pos',[10,10,900,600],'renderer','painters');
subplot(1,2,1)
errorbar([-2,-1,0,1,2],xx,xx_std,'LineWidth',2,'LineStyle','none');
xlabel('\Delta CH6 [A]','FontSize',18);
ylabel('x [pixels]','FontSize',18)
subplot(1,2,2)
errorbar([-2,-1,0,1,2],yx,yx_std,'LineWidth',2,'LineStyle','none');
xlabel('\Delta CH6 [A]','FontSize',18);
ylabel('y [pixels]','FontSize',18)
sgtitle('CH6 response on DM4','FontSize',25)
%%
figure('pos',[10,10,900,600],'renderer','painters');
subplot(1,2,1)
errorbar([-2,-1,0,1,2],xy,xy_std,'LineWidth',2,'LineStyle','none');
xlabel('\Delta CV6 [A]','FontSize',18);
ylabel('x [pixels]','FontSize',18)
subplot(1,2,2)
errorbar([-2,-1,0,1,2],yy,yy_std,'LineWidth',2,'LineStyle','none');
xlabel('\Delta CV6 [A]','FontSize',18);
ylabel('y [pixels]','FontSize',18)
sgtitle('CV6 response on DM4','FontSize',25)
