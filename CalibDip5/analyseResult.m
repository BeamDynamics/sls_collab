clear all
addpath('..');
fnames_h = {...
    'CalDip5_CH7_m3A_06-Dec-2020_15-03-14';...
    'CalDip5_CH7_m1A_06-Dec-2020_14-55-57';...
    'CalDip5_NominalSteerers_06-Dec-2020_14-49-28';...
    'CalDip5_CH7_p1A_06-Dec-2020_14-54-39';...
    'CalDip5_CH7_p3A_06-Dec-2020_14-57-34'};


fnames_v = {...
    'CalDip5_CV7_m2A_06-Dec-2020_15-10-45';...
    'CalDip5_CV7_m1A_06-Dec-2020_15-07-45';...
    'CalDip5_NominalSteerers_06-Dec-2020_14-49-28';...
    'CalDip5_CV7_p1A_06-Dec-2020_15-06-28';...
    'CalDip5_CV7_p2A_06-Dec-2020_15-09-21'};

for i = 1:numel(fnames_h)
   
    i
    load(fnames_h{i})
    for a = 1:numel(d5)
        [x_tmp(a),sigmax_tmp(a),~,~] = gaussianfit1D(sum(double(d5{a})),0);
        [y_tmp(a),sigmay_tmp(a),~,~] = gaussianfit1D(sum(double(d5{a}),2),0);
    end
    xx(i) = mean(rmoutliers(x_tmp));% Horizontal movement due to horizontal steerer
    xx_std(i) = std(rmoutliers(x_tmp));
    yx(i) = mean(rmoutliers(y_tmp));% Vertical movement due to horizontal steerer
    yx_std(i) = std(rmoutliers(y_tmp));
end

for i = 1:numel(fnames_v)
   
    i
    load(fnames_v{i})
    for a = 1:numel(d5)
        [x_tmp(a),sigmax_tmp(a),~,~] = gaussianfit1D(sum(double(d5{a})),0);
        [y_tmp(a),sigmay_tmp(a),~,~] = gaussianfit1D(sum(double(d5{a}),2),0);
    end
    xy(i) = mean(rmoutliers(x_tmp));% Horizontal movement due to vertical steerer
    xy_std(i) = std(rmoutliers(x_tmp));
    yy(i) = mean(rmoutliers(y_tmp));% Vertical movement due to vertical sterrer
    yy_std(i) = std(rmoutliers(y_tmp));
end
%%
figure('pos',[10,10,900,600],'renderer','painters');
subplot(1,2,1)
errorbar([-3,-1,0,1,3],xx,xx_std,'LineWidth',2,'LineStyle','none');
xlabel('\Delta CH7 [A]','FontSize',18);
ylabel('x [pixels]','FontSize',18)
subplot(1,2,2)
errorbar([-3,-1,0,1,3],yx,yx_std,'LineWidth',2,'LineStyle','none');
xlabel('\Delta CH7 [A]','FontSize',18);
ylabel('y [pixels]','FontSize',18)
sgtitle('CH7 response on DM5','FontSize',25)
%%
figure('pos',[10,10,900,600],'renderer','painters');
subplot(1,2,1)
errorbar([-2,-1,0,1,2],xy,xy_std,'LineWidth',2,'LineStyle','none');
xlabel('\Delta CV7 [A]','FontSize',18);
ylabel('x [pixels]','FontSize',18)
subplot(1,2,2)
errorbar([-2,-1,0,1,2],yy,yy_std,'LineWidth',2,'LineStyle','none');
xlabel('\Delta CV7 [A]','FontSize',18);
ylabel('y [pixels]','FontSize',18)
sgtitle('CV7 response on DM5','FontSize',25)
