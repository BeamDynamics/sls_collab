clear all;
% load('data_disptl2_pm3kHz_7freq_emitExchange.mat')
load('data_disptl2_pm3kHz_7freq_tunesuncoupled')


for i = 1:size(orbith,3)
    xtmp = orbith(:,:,i);
    ytmp = orbitv(:,:,i);
    
    x(:,i) = nanmean(xtmp,2);
    x_std(:,i) = nanstd(xtmp');
    y(:,i) = nanmean(ytmp,2);
    y_std(:,i) = nanstd(ytmp'); 
end

frf = f(3);
dfrf = f-frf;

alphac = 9.6e-3;
dp = dfrf/frf/alphac; %energy spread

% Fit a straight line for each BPM
for i = 1:size(x,1)
    fitx{i} = fit(dp',x(i,:)','poly1','Weights',1./x_std(i,:).^2');
    fity{i} = fit(dp',y(i,:)','poly1','Weights',1./y_std(i,:).^2');
    Dx(i) = fitx{i}.p1;
    Dy(i) = fity{i}.p1;
end

%% Go through the cameras 
addpath('..')
for a = 1:size(im,2)% iterate through the rf settings
    a
    for b = 1:size(im,1)% iterate through the samples
        b
        [x_tmp(:,b),sigmax_tmp(:,b),y_tmp(:,b),sigmay_tmp(:,b)] = analyzeBeamProfile(im(b,a));
    end
    x_cam(:,a) = mean(rmoutliers(x_tmp),2);
    x_cam_std(:,a) = std(rmoutliers(x_tmp'));
    y_cam(:,a) = mean(rmoutliers(y_tmp),2);
    y_cam_std(:,a) = std(rmoutliers(y_tmp'));
end
%% Analyze dispersion of cameras
calibx = 1;%calibration factor in x
caliby = 1;%calibration factor in y
x_cam = x_cam*calibx;
y_cam = y_cam*caliby;
x_cam_std = x_cam_std*calibx;
y_cam_std = y_cam_std*caliby;

for i = 1:size(x_cam,1)
    fitx_cam{i} = fit(dp',x_cam(i,:)','poly1','Weights',1./x_cam_std(i,:).^2');
    fity_cam{i} = fit(dp',y_cam(i,:)','poly1','Weights',1./y_cam_std(i,:).^2');
    Dx_cam(i) = fitx_cam{i}.p1;
    Dy_cam(i) = fity_cam{i}.p1;
end

%%
figure('pos',[10,10,900,600],'renderer','painters');
hold on;
for i = 2:size(x,1)
    subplot(3,3,i-1)
    hold on
    errorbar(dp,x(i,:),x_std(i,:))
    xfit = fitx{i}(dp);
    plot(dp,xfit);
    xlabel('\Delta p/p')
    ylabel('\Delta x [m]')
    title(['BPM #',num2str(i),'. \eta_x = ',num2str(round(Dx(i),2)),'m'])
end

figure('pos',[10,10,900,600],'renderer','painters');
hold on;
for i = 2:size(x,1)
    subplot(3,3,i-1)
    hold on
    errorbar(dp,y(i,:),y_std(i,:))
    yfit = fity{i}(dp);
    plot(dp,yfit);
    
    xlabel('\Delta p/p')
    ylabel('\Delta y [m]')
    title(['BPM #',num2str(i),'. \eta_y = ',num2str(round(Dy(i),2)),'m'])
end
