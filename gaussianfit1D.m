function [x0,sigma,A,B]=gaussianfit1D(DATI1D, plotting)
 %{
Original code by Guiseppe Penco, Elettra
Takes the projected profiles (x or y) and returns: 
Gauss(1): centroid location
Gauss(2): beam size
Gauss(3): Amplitude scaling of the gaussian
Gauss(4): Offset
%}
 
%% creo le funzioni di appoggio:
gaussiana=vectorize(inline('A+B*exp((-(x-x0).^2)/(2*Sigma^2))'));
GSOB=vectorize(inline('exp((-(x-x0).^2)/(2*Sigma^2))'));
DERX0=inline('sum((VECT-x).*(B*exp((-(VECT-x).^2)/(2*S^2)) - Dat ).*exp((-(VECT-x).^2)/(2*S^2)))');
DERSIGMA=inline('sum((((VECT-x0).^2)./(S^3)).*(B*exp((-(VECT-x0).^2)/(2*S^2)) - Dat ).*exp((-(VECT-x0).^2)/(2*S^2)))');
 
Dimensione_Indovina_Offset=20;
Dimensione_Indovina_Media=3;
Forza_Filtro_Mediano=1;
Soglia_Convergenza=10^-8;
Soglia_Convergenza_DER=10^-9;
Massimo_Cicli=50;
 
N=length(DATI1D);
 
%ApplicaMediano:
for X=1:N-Forza_Filtro_Mediano+1
DATI(X)=median(DATI1D(X:X+Forza_Filtro_Mediano-1));
end
 
%Ristora dimensioni originali
DATI=[ones(1,floor(Forza_Filtro_Mediano/2))*DATI(1),DATI,ones(1,floor(Forza_Filtro_Mediano/2))*DATI(N-Forza_Filtro_Mediano+1)];
if ~mod(Forza_Filtro_Mediano,2)
    DATI=DATI(1:N);
end
 
Indovina_Offset=min(mean(DATI(1,Dimensione_Indovina_Offset)),mean(DATI(N-Dimensione_Indovina_Offset),N));
 
for X=1:N-Dimensione_Indovina_Media+1
medie(X)=mean(DATI(X:X+Dimensione_Indovina_Media-1));
end
 
 
Indovina_Media=(Dimensione_Indovina_Media-1)/2+find(medie==max(medie));
Indovina_Media=Indovina_Media(1);
Indovina_c_moltiplicativa=medie(find(medie==max(medie)))-Indovina_Offset;
Indovina_c_moltiplicativa=Indovina_c_moltiplicativa(1);
VN=1:N;
varianza=(2*((VN - Indovina_Media).^2)*((transpose(DATI-Indovina_Offset)>0).*(transpose(DATI-Indovina_Offset))) - (DATI(1)-Indovina_Offset)*(1-Indovina_Media)^2 -(DATI(N)-Indovina_Offset)*(N-Indovina_Media)^2)/2;
Indovina_Sigma= ( varianza/(Indovina_c_moltiplicativa*sqrt(2*pi)) )^(1/3);
 
Funzionale_Errore_VOLD=sum( (Indovina_Offset + Indovina_c_moltiplicativa*exp((-(VN-Indovina_Media).^2)/(2*Indovina_Sigma^2)) - DATI).^2 );
Funzionale_Errore_OLD=sum( (Indovina_Offset + Indovina_c_moltiplicativa*exp((-(VN-Indovina_Media).^2)/(2*Indovina_Sigma^2)) - DATI).^2 );
Derivata_Funzionale_Errore=+inf;
indice=0;
 
 
while (Derivata_Funzionale_Errore>Soglia_Convergenza_DER)
%for indice=1:10
indice=indice+1;
 
NGS=fzero(@(x) DERSIGMA(Indovina_c_moltiplicativa,DATI-Indovina_Offset,x,VN,Indovina_Media),Indovina_Sigma);
Funzionale_Errore=sum( (Indovina_Offset + Indovina_c_moltiplicativa*exp((-(VN-Indovina_Media).^2)/(2*NGS^2)) - DATI).^2 );
if(Funzionale_Errore<Funzionale_Errore_OLD)
    Indovina_Sigma=NGS;
    Funzionale_Errore_OLD=Funzionale_Errore;
end
 
NGM=fzero(@(x) DERX0(Indovina_c_moltiplicativa,DATI,Indovina_Sigma,VN,x),Indovina_Media);

Funzionale_Errore=sum( (Indovina_Offset + Indovina_c_moltiplicativa*exp((-(VN-NGM).^2)/(2*Indovina_Sigma^2)) - DATI).^2 );
if(Funzionale_Errore<Funzionale_Errore_OLD)
    Indovina_Media=NGM;
    Funzionale_Errore_OLD=Funzionale_Errore;
end
 
Matrice_Minimizza_AB=[ones(N,1),GSOB(Indovina_Sigma,transpose(1:N),Indovina_Media)];
AB=pinv(transpose(Matrice_Minimizza_AB)*Matrice_Minimizza_AB)*(transpose(Matrice_Minimizza_AB)*transpose(DATI));
Funzionale_Errore=sum( (AB(1) + AB(2)*exp((-(VN-Indovina_Media).^2)/(2*Indovina_Sigma^2)) - DATI).^2 );
if(Funzionale_Errore<Funzionale_Errore_OLD)
    Indovina_Offset=AB(1);
    Indovina_c_moltiplicativa=AB(2);
    Funzionale_Errore_OLD=Funzionale_Errore;
end
 
Funzionale_Errore=sum( (Indovina_Offset + Indovina_c_moltiplicativa*exp((-(VN-Indovina_Media).^2)/(2*Indovina_Sigma^2)) - DATI).^2 );
Derivata_Funzionale_Errore=(Funzionale_Errore_VOLD-Funzionale_Errore)/Funzionale_Errore_VOLD;
Funzionale_Errore_VOLD=Funzionale_Errore;
if(Funzionale_Errore<Soglia_Convergenza)
    break
end
if(indice>Massimo_Cicli)
    break
end
 
end %del while
 
A=Indovina_Offset;
B=Indovina_c_moltiplicativa;
x0=Indovina_Media;
sigma=Indovina_Sigma;
% Gauss=[x0,sigma,A,B];
 
%%%% PLOT
if plotting==1,
    figure
    plot(VN, gaussiana(A,B,sigma,VN,x0),'-r', VN, DATI1D,'.');
    %plot(VN, DATI1D,'.');
    
end
